<?php

class CourseObjectScoNode extends CourseObjectNode {

  function access($op = 'view', $account = NULL) {
    $access = parent::access($op, $account);
    if ($access) {
      $node = $this->getNode();
      if (!property_exists($node, 'sco_manifest') || !property_exists($node, 'sco_path')) {
        // No content for this SCO Node; disallow view or take access
        if ($op == 'view' || $op == 'take') {
          $access = FALSE;
        }
      }
    }
    return $access;
  }

  function getNodeTypes() {
    return array('sco_node');
  }

  function getTakeUrl() {
    // Get our query parameters to pass on, as appropriate
    $query = $_GET;
    unset($query['q']);
    if ($this->getInstanceId()) {
      return url("node/{$this->node->nid}/play", array('query' => $query));
    }
  }

  /**
   * Let the user know if they have an SCO Node without any SCORM content.
   */
  public function getWarnings() {
    $warnings = parent::getWarnings();

    if ($this->getInstanceId()) {
      if (!property_exists($this->node, 'sco_path') || empty($this->node->sco_path)) {
        $warnings[] = t('This SCO Node does not have any SCORM content. Please !link.', array('!link' => l('upload a SCORM package', 'node/' . $this->getInstanceId() . '/edit')));
      }
    }

    return $warnings;
  }

  /**
   * Marks a user's fulfillment record for this object complete if the user
   * passed the SCO Node content.
   */
  function grade($account, $attempt) {
    $nid = (int) $this->getInstanceId();
    $fulfillment = $this->getFulfillment();

    $attempt_ids = (array) $fulfillment->getOption('sco_node_attempt_ids');
    $attempt_ids[$attempt->id] = $attempt->id;
    $fulfillment->setOption('sco_node_attempt_ids', $attempt_ids);

    if (in_array($attempt->status, array('completed', 'passed'))) {
      $fulfillment->setGrade($attempt->score)->setComplete()->save();
    }
    else {
      $fulfillment->setGrade($attempt->score)->save();
    }
  }

  /**
   * @TODO
   * Is this object graded?
   *
   * Returning TRUE here will cause some more configurations to show on the
   * object's form.
   *
   * @return bool
   */
  function isGraded() {
    return FALSE;
  }

}
